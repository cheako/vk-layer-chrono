use std::os::unix::net;

pub fn go(
    tmp_path: impl AsRef<std::path::Path>,
    path: impl AsRef<std::path::Path>,
    mut f: impl FnMut(net::UnixStream) -> std::thread::JoinHandle<std::io::Result<()>>,
) -> std::io::Result<()> {
    let _ = std::fs::remove_file(&tmp_path);
    let listener = net::UnixListener::bind(&tmp_path)?;
    std::fs::rename(tmp_path, path)?;

    // accept connections and process them, spawning a new thread for each one
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                f(stream).join().expect("Join failed")?;
            }
            Err(_) => {
                /* connection failed */
                break;
            }
        }
    }
    Ok(())
}
