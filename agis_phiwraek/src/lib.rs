use rustix::fd as rfd;
use rustix::fd::AsFd;
use rustix::io as rio;
use std::io;
use std::io::BufWriter;
use std::io::Write;
use std::process::Stdio;
use std::thread as t;

/// ```
/// #![feature(duration_constants)]
///
/// use thread_tryjoin::TryJoinHandle;
/// use std::process as proc;
/// use std::thread as t;
///
/// let orig_hook = std::panic::take_hook();
/// std::panic::set_hook(Box::new(move |panic_info| {
/// // invoke the default handler and exit the process
/// orig_hook(panic_info);
///     std::process::exit(1);
/// }));
///
/// t::spawn(|| agis_phiwraek::go("/tmp/a", "/tmp/b", || {
///     let mut cmd = proc::Command::new("/bin/bash"); cmd.arg("-"); cmd
/// }).unwrap()).try_timed_join(std::time::Duration::SECOND * 30).unwrap();
/// ```
pub fn go(
    tmp_path: impl AsRef<std::path::Path>,
    path: impl AsRef<std::path::Path>,
    mut shell: impl FnMut() -> std::process::Command,
) -> std::io::Result<()> {
    sock::go(
        tmp_path,
        path,
        move |stream| -> t::JoinHandle<std::io::Result<()>> {
            use command_fds::{CommandFdExt, FdMapping};
            use rustix::fd::{AsRawFd, IntoFd};

            let (stdout_child, stdout) = match rustix::io::pipe() {
                Ok(x) => x,
                x => return t::spawn(|| x.map(|_| ()).map_err(Into::into)),
            };

            let stderr = match rustix::io::dup(&stdout) {
                Ok(x) => x,
                x => return t::spawn(|| x.map(|_| ()).map_err(Into::into)),
            };

            let mut child = match shell()
                .stdin(Stdio::piped())
                .fd_mappings(vec![
                    FdMapping {
                        parent_fd: stdout.as_raw_fd(),
                        child_fd: linux_raw_sys::general::STDOUT_FILENO as _,
                    },
                    FdMapping {
                        parent_fd: stderr.as_raw_fd(),
                        child_fd: linux_raw_sys::general::STDERR_FILENO as _,
                    },
                ])
                .unwrap()
                .spawn()
            {
                Ok(x) => x,
                x => return t::spawn(|| x.map(|_| ()).map_err(Into::into)),
            };

            let stream_fd = stream.try_clone().unwrap().into_fd();
            if let Err(x) = rio::ioctl_fionbio(&stream_fd, true) {
                return t::spawn(move || Err(x).map_err(Into::into));
            }
            if let Err(x) = rio::ioctl_fionbio(&stdout_child, true) {
                return t::spawn(move || Err(x).map_err(Into::into));
            }
            t::spawn(move || {
                read2(
                    stdout_child.into(),
                    stream,
                    stream_fd,
                    child.stdin.take().unwrap(),
                )
            })
        },
    )
}

fn read2(
    s1: rfd::OwnedFd,
    d1: impl io::Write,
    s2: rfd::OwnedFd,
    d2: impl io::Write,
) -> io::Result<()> {
    let mut fds = [
        rio::PollFd::new(&s1, rio::PollFlags::IN),
        rio::PollFd::new(&s2, rio::PollFlags::IN),
    ];
    let mut writers: [Box<dyn io::Write>; 2] = [
        Box::new(BufWriter::with_capacity(4096, d1)),
        Box::new(BufWriter::with_capacity(4096, d2)),
    ];

    loop {
        rustix::io::poll(&mut fds, -1)?;

        for (fd, writer) in fds
            .iter_mut()
            .zip(writers.iter_mut())
            .filter(|(fd, _)| fd.revents() != rio::PollFlags::empty())
        {
            let mut buf = [0u8; 4096];
            let len = rio::read(fd.as_fd(), &mut buf)?;
            if len != 0 {
                let mut off = writer.write(&buf[..=len])?;
                while off < len {
                    off += writer.write(&buf[off..=len])?;
                }
                writer.flush()?;
            }
            fd.clear_revents();
        }
    }
}
